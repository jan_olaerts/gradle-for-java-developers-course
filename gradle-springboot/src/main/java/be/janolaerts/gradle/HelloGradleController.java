package be.janolaerts.gradle;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloGradleController {

    @GetMapping("/hello")
    public String hello() {
        return "Spring Boot with Gradle is super easy";
    }
}
