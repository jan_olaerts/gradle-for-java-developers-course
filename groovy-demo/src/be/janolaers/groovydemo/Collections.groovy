package be.janolaers.groovydemo

LinkedList l = [1,2,3]
println l
println l.class

l << 4 // adds element to the list
println l

l += [5,6,7] // adding elements to the list
println l

println l-[3,5] // removes 3 and 5 from the list

l.each {println it} // it referes to the current element
l.reverseEach {println it}
l.eachPermutation {println it}

s = ['java', 'js', 'python', 'js'] as Set
println s
println s.class

m = [courseName:'Gradle', rating:5, price:20]
println m
m.each {k,v ->
    println k
    println v
}
println m.courseName
println m['courseName']
println m.get('courseName')

m.review = "It's awesome" // adds new key-value pair
println m