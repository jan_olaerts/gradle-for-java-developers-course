package be.janolaers.groovydemo

float a = 10.24F

println a
println a.class

s = 'hello'
println s

name = 'Jan'
s = "hello ${name}" // "" => groovy string (allows for variables)
println s

s = '''
This allows for a
string on multiple
lines
'''
println(s)

emailPattern = /^[a-zA-Z0-9.!#$%&’*+\=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
println "test@gmail.com" ==~ emailPattern // ==~ pattern operator
println emailPattern.getClass() // java.lang.String