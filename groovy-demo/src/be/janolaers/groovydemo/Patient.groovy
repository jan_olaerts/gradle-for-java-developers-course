package be.janolaers.groovydemo

class Patient {
    String firstName, lastName
    int age
    static hospitalCode = 'Best Hospital'

    public static void main(String[] args) {
        Patient p = new Patient(firstName:'John', lastName:'Bailey', age:40)
        p.setLastName('Buffer')
        println p.firstName + " " + p.lastName + " " + p.age // groovy generates getters and setters in the background
        Patient.display()
    }

    void setLastName(lastName) { // overrides generated setter method
        if(lastName == null) {
            throw new IllegalArgumentException("Last name cannot be null");
        }
        this.lastName = lastName
    }

    static void display() {
        println hospitalCode
    }
}