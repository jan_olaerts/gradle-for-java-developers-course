package be.janolaers.groovydemo;

c = { n=2 -> // 2 is default value
    println(n % 2 == 0 ? "even" : "uneven")
}

c.call()

4.times { n -> print n }
4.times {print it } // prints the same as the closure above