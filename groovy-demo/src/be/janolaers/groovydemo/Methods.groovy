package be.janolaers.groovydemo

int product(int x=3, int y) {
    x*y // return can be left out
}
result = product 5
println result

void display(Map productDetails) {
    println productDetails.name
    println productDetails.price
}
display([name:'IPhone', price:1000])
display (name:'IPhone', price:1000) // works without []
display name:'IPhone', price:1000 // works without ()